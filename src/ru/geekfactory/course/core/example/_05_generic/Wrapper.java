package ru.geekfactory.course.core.example._05_generic;

public class Wrapper {

    private Object value;

    public Wrapper(Object value) {
        this.value = value;
    }

    public Object getValue() {
        return value;
    }


    public static void main(String[] args) {
        Wrapper intWrapper = new Wrapper(100);
        int intValue = (Integer) intWrapper.getValue();

        Wrapper strWrapper = new Wrapper("string");
        String strValue = (String) strWrapper.getValue();

        intWrapper = strWrapper;
        int intValueRte = (Integer) intWrapper.getValue();
    }
}
