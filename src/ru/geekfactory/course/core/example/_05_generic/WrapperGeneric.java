package ru.geekfactory.course.core.example._05_generic;

public class WrapperGeneric <T> {

    private T value;

    public WrapperGeneric(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }


    public static void main(String[] args) {
        WrapperGeneric<Integer> intWrapper = new WrapperGeneric<>(100);
        int intValue = intWrapper.getValue();

        WrapperGeneric<String> strWrapper = new WrapperGeneric<>("string");
        String strValue = strWrapper.getValue();

//        intWrapper = strWrapper;
        int intValueRte = intWrapper.getValue();


        WrapperGeneric rawWrapper = new WrapperGeneric(10);
        Integer value = (Integer) rawWrapper.getValue();
    }
}

