package ru.geekfactory.course.core.example._05_generic;

public class SumCalc<T extends Number> {

    private T[] values;

    SumCalc(T... values) {
        this.values = values;
    }

    public double sum() {
        double sum = 0.0;
        for (int i=0; i < values.length; i++) {
            sum += values[i].doubleValue();
        }
        return sum;
    }

    boolean sameSum(SumCalc<?> ob) {
        return this.sum() == ob.sum();
    }

    public T[] getValues() {
        return values;
    }

    public static void main(String[] args) {

    }

}
