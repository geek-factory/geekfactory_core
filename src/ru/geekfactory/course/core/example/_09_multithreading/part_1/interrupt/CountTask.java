package ru.geekfactory.course.core.example._09_multithreading.part_1.interrupt;

public class CountTask implements Runnable {

    @Override
    public void run() {
        int i = 0;
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println(i++);

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }

        System.out.println("Thread finished");
    }
}
