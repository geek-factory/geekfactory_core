package ru.geekfactory.course.core.example._09_multithreading.part_1.wait_notify;

public class Producer implements Runnable {

    private final ValueHolder valueHolder;

    public Producer(ValueHolder valueHolder) {
        this.valueHolder = valueHolder;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            valueHolder.put(i);

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
