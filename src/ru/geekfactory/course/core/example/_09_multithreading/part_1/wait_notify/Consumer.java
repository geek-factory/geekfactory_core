package ru.geekfactory.course.core.example._09_multithreading.part_1.wait_notify;

public class Consumer implements Runnable {

    private final ValueHolder valueHolder;

    public Consumer(ValueHolder valueHolder) {
        this.valueHolder = valueHolder;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            valueHolder.get();

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
