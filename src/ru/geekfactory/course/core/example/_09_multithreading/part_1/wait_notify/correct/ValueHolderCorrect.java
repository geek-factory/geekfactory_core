package ru.geekfactory.course.core.example._09_multithreading.part_1.wait_notify.correct;

import ru.geekfactory.course.core.example._09_multithreading.part_1.wait_notify.ValueHolder;

public class ValueHolderCorrect implements ValueHolder {

    private int value;
    private boolean isValueSet = false;

    public synchronized int get() {
        while (!isValueSet) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("GET: " + value);
        isValueSet = false;
        notifyAll();

        return value;
    }

    public synchronized void put(int value) {
        while (isValueSet) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        this.value = value;
        System.out.println("SET: " + value);
        isValueSet = true;
        notifyAll();
    }
}
