package ru.geekfactory.course.core.example._09_multithreading.part_1.wait_notify;

import ru.geekfactory.course.core.example._09_multithreading.part_1.wait_notify.correct.ValueHolderCorrect;

public class Demo {

    public static void main(String[] args) {
//        ValueHolder valueHolder = new ValueHolderNotCorrect();
        ValueHolder valueHolder = new ValueHolderCorrect();

        new Thread(new Producer(valueHolder)).start();
        new Thread(new Consumer(valueHolder)).start();
    }
}
