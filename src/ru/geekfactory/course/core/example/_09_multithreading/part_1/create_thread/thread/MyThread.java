package ru.geekfactory.course.core.example._09_multithreading.part_1.create_thread.thread;

public class MyThread extends Thread {

    public MyThread(String name) {
        super(name);
    }

    @Override
    @SuppressWarnings("Duplicates")
    public void run() {
        System.out.println(Thread.currentThread().getName() + " start");

        try {
            for (int i = 0; i < 5; i++) {
                System.out.println(Thread.currentThread().getName() + " " + i);
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(Thread.currentThread().getName() + " finish");
    }
}
