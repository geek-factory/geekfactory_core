package ru.geekfactory.course.core.example._09_multithreading.part_1.create_thread.thread;

public class CreateThreadTreadDemo {

    public static void main(String[] args) throws InterruptedException {
        // create threads
        Thread runTread_1 = new MyThread("runTread_1");
        Thread runTread_2 = new MyThread("runTread_2");
        Thread runTread_3 = new MyThread("runTread_3");

        // start threads
        runTread_1.start();
        runTread_2.start();
        runTread_3.start();

        // loop in main thread
        for (int i = 0; i < 5; i++) {
            System.out.println(Thread.currentThread().getName() + " " + i);
            Thread.sleep(500);
        }

        System.out.println(Thread.currentThread().getName() + " finish");
    }
}
