package ru.geekfactory.course.core.example._09_multithreading.part_1.wait_notify.not_correct;

import ru.geekfactory.course.core.example._09_multithreading.part_1.wait_notify.ValueHolder;

public class ValueHolderNotCorrect implements ValueHolder {

    private int value;

    public synchronized int get() {
        System.out.println("GET: " + value);
        return value;
    }

    public synchronized void put(int value) {
        this.value = value;
        System.out.println("SET: " + value);
    }
}
