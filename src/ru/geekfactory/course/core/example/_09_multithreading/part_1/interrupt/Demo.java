package ru.geekfactory.course.core.example._09_multithreading.part_1.interrupt;

public class Demo {

    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(new CountTask());
        thread.start();

        Thread.sleep(3000);

        thread.interrupt();
    }
}
