package ru.geekfactory.course.core.example._09_multithreading.part_1.volat;

public class VolatileDemo {

    private static /*volatile*/ boolean STOP_SIGNAL = true;

    public static void main(String[] args) throws InterruptedException {

        // Использование анонимных классов
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (STOP_SIGNAL) {}
                System.out.println("1 - value = " + STOP_SIGNAL);
            }
        }).start();

        Thread.sleep(100);

        new Thread(new Runnable() {
            @Override
            public void run() {
                STOP_SIGNAL = false;
                System.out.println("2 - value = " + STOP_SIGNAL);
            }
        }).start();
    }



}
