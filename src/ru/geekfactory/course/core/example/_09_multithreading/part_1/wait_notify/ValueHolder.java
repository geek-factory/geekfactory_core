package ru.geekfactory.course.core.example._09_multithreading.part_1.wait_notify;

public interface ValueHolder {

    int get();

    void put(int value);
}
