package ru.geekfactory.course.core.example._09_multithreading.part_1.deadlock;

public class DeadLockDemo {

    public static void main(String[] args) {
        Object lock1 = new Object();
        Object lock2 = new Object();

        Thread thread1 = new Thread(new Task(lock1, lock2));
        Thread thread2 = new Thread(new Task(lock2, lock1));

        thread1.start();
        thread2.start();
    }


    private static class Task implements Runnable {

        private final Object lock1;
        private final Object lock2;

        public Task(Object lock1, Object lock2) {
            this.lock1 = lock1;
            this.lock2 = lock2;
        }

        @Override
        public void run() {
            System.out.println(currentThreadName() + " try get lock1");
            sleep(500);
            synchronized (lock1) {
                System.out.println(currentThreadName() + " success get lock1");
                System.out.println(currentThreadName() + " try get lock2");
                sleep(500);
                synchronized (lock2) {
                    System.out.println(currentThreadName() + " success get lock2");
                }
            }
        }
    }

    private static String currentThreadName() {
        return Thread.currentThread().getName();
    }

    private static void sleep(int mills) {
        try {
            Thread.sleep(mills);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
