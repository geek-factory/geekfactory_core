package ru.geekfactory.course.core.example._09_multithreading.part_1.sync;

public class Wrapper {

    private int value;

    public Wrapper(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public /*synchronized*/ void inc() {
        this.value++;
    }

    public /*synchronized*/ void dec() {
        this.value--;
    }

    @Override
    public String toString() {
        return "Wrapper{" +
                "value=" + value +
                '}';
    }
}
