package ru.geekfactory.course.core.example._09_multithreading.part_1.is_alive_join;

import ru.geekfactory.course.core.example._09_multithreading.part_1.create_thread.runnable.MyRunnable;

@SuppressWarnings("Duplicates")
public class IsAliveJoinDemo {

    public static void main(String[] args) throws InterruptedException {
        // Create  threads
        Thread runTread_1 = new Thread(new MyRunnable(), "runThread_1");
        Thread runTread_2 = new Thread(new MyRunnable(), "runThread_2");
        Thread runTread_3 = new Thread(new MyRunnable(), "runThread_3");

        printIsAliveForThreads("Create", runTread_1, runTread_2, runTread_3);

        // Start threads
        runTread_1.start();
        runTread_2.start();
        runTread_3.start();

        printIsAliveForThreads("Start", runTread_1, runTread_2, runTread_3);

        runTread_1.join();
        runTread_2.join();
        runTread_3.join();

        printIsAliveForThreads("Join", runTread_1, runTread_2, runTread_3);

        System.out.println(Thread.currentThread().getName() + " finish");
    }

    private static void printIsAliveForThreads(String prefix, Thread... threads) {
        for (Thread thread : threads) {
            System.out.println(prefix + " " + thread.getName() + " - isAlive: " + thread.isAlive());
        }

    }
}
