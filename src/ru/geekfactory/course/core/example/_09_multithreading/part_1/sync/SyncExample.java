package ru.geekfactory.course.core.example._09_multithreading.part_1.sync;

/**
 * Инкрементим 11 раз в отдельном потоке
 * Декремнтим 10 раз в отдельном потоке
 *
 * Ожидание 101
 */
@SuppressWarnings("Duplicates")
public class SyncExample {

    private static final int INC_COUNT = 100_001;
    private static final int DEC_COUNT = 100_000;

    public static void main(String[] args) throws InterruptedException {
        Wrapper wrapper = new Wrapper(100);

        Thread incThread = new Thread(new IncTask(wrapper));
        Thread decThread = new Thread(new DecTask(wrapper));

        incThread.start();
        decThread.start();

        incThread.join();
        decThread.join();

        System.out.println("MAIN wrapper = " + wrapper);

    }

    private static class DecTask implements Runnable {

        private final Wrapper wrapper;

        private DecTask(Wrapper i) {
            this.wrapper = i;
        }

        @Override
        public void run() {
            synchronized (wrapper) {
                for (int j = 0; j < DEC_COUNT; j++) {
                    wrapper.dec();
                }
                System.out.println("-- wrapper = " + wrapper);
            }
        }
    }

    private static class IncTask implements Runnable {

        private final Wrapper wrapper;

        public IncTask(Wrapper i) {
            this.wrapper = i;
        }

        @Override
        public void run() {
            synchronized (wrapper) {
                for (int j = 0; j < INC_COUNT; j++) {
                    wrapper.inc();
                }
                System.out.println("++ wrapper = " + wrapper);
            }
        }
    }
}
