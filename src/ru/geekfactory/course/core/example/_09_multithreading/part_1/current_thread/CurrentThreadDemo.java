package ru.geekfactory.course.core.example._09_multithreading.part_1.current_thread;

public class CurrentThreadDemo {

    public static void main(String[] args) {
        Thread currentThread = Thread.currentThread();

        System.out.println(currentThread);

        currentThread.setName("changed name");

        System.out.println(currentThread);
    }
}
