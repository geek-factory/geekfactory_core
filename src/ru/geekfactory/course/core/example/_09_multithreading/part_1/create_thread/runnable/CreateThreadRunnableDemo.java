package ru.geekfactory.course.core.example._09_multithreading.part_1.create_thread.runnable;

public class CreateThreadRunnableDemo {

    public static void main(String[] args) throws InterruptedException {
        // Create  threads
        Thread runTread_1 = new Thread(new MyRunnable(), "runThread_1");
        Thread runTread_2 = new Thread(new MyRunnable(), "runThread_2");
        Thread runTread_3 = new Thread(new MyRunnable(), "runThread_3");

        // Start threads
        runTread_1.start();
        runTread_2.start();
        runTread_3.start();

        // Start loop in main thread
        for (int i = 0; i < 5; i++) {
            System.out.println(Thread.currentThread().getName() + " " + i);
            Thread.sleep(500);
        }

        System.out.println(Thread.currentThread().getName() + " finish");
    }
}
