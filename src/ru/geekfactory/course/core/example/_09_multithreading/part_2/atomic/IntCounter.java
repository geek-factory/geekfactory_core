package ru.geekfactory.course.core.example._09_multithreading.part_2.atomic;

public class IntCounter implements Counter {

    private int i = 0;

    public void increment() {
        i++;
    }

    public int getI() {
        return i;
    }
}
