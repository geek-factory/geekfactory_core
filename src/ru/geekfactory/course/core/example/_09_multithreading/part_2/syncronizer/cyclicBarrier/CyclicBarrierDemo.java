package ru.geekfactory.course.core.example._09_multithreading.part_2.syncronizer.cyclicBarrier;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class CyclicBarrierDemo implements Runnable {

    private int i;
    private CyclicBarrier barrier;

    public CyclicBarrierDemo(CyclicBarrier barrier, int i) {
        this.i = i;
        this.barrier = barrier;
    }

    @Override
    public void run() {
        try {
            int time = (int) (10000 * Math.random());
            System.out.println(LocalDateTime.now() + " Executing " + i + ":" + time);
            Thread.sleep(time);
            barrier.await();
            System.out.println(LocalDateTime.now() + " Barrier reached " + i);
        } catch (InterruptedException | BrokenBarrierException e) {
            Thread.currentThread().interrupt();
        }
    }

    public static void main(String[] args) throws Exception {
        CyclicBarrier barrier = new CyclicBarrier(5);

        List<Thread> threadList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Thread thread = new Thread(new CyclicBarrierDemo(barrier, i));
            threadList.add(thread);
            thread.start();
        }

        Thread.sleep(15000);

//        threadList.forEach(Thread::interrupt);
    }
}
