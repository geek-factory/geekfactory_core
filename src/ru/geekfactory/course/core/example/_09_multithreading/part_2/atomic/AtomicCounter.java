package ru.geekfactory.course.core.example._09_multithreading.part_2.atomic;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicCounter implements Counter {

    private AtomicInteger atomicInteger = new AtomicInteger(0);

    @Override
    public void increment() {
        atomicInteger.incrementAndGet();
    }

    @Override
    public int getI() {
        return atomicInteger.get();
    }
}
