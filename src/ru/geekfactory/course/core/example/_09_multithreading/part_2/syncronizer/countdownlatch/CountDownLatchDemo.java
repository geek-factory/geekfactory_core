package ru.geekfactory.course.core.example._09_multithreading.part_2.syncronizer.countdownlatch;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class CountDownLatchDemo {

    public static void main(String[] args) throws Exception {
        final CountDownLatch latch = new CountDownLatch(5);

        Thread thread = new Thread("Additional Thread") {
            public void run() {
                try {
                    while (!isInterrupted()) {
                        System.out.println(Thread.currentThread().getName() + " Executed " + latch.getCount());
                        latch.countDown(); //5 4 3 2 1 0
                        Thread.sleep(1000);
                    }
                } catch (InterruptedException e) {
                    interrupt();
                }
            }
        };

        thread.start();

        if (!latch.await(2, TimeUnit.SECONDS)) {
            System.out.println("Fail");
        }

        thread.interrupt();
    }
}
