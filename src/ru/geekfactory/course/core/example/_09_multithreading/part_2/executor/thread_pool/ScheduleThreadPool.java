package ru.geekfactory.course.core.example._09_multithreading.part_2.executor.thread_pool;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ScheduleThreadPool {

    public static void main(String[] args) throws InterruptedException {
        ScheduledExecutorService scheduledThreadPool = Executors.newScheduledThreadPool(5);

        Runnable task = new Runnable() {
            @Override
            public void run() {
                System.out.println("Executed");
            }
        };

        scheduledThreadPool.scheduleWithFixedDelay(task, 1, 5, TimeUnit.SECONDS);
        Thread.sleep(17000);
        scheduledThreadPool.shutdown();
    }
}
