package ru.geekfactory.course.core.example._09_multithreading.part_2.executor.callable;

import java.util.concurrent.Callable;

public class Summator implements Callable<Integer> {

    private int startIndex;
    private int endIndex;
    private byte[] array;

    public Summator(int startIndex, int endIndex, byte[] array) {
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.array = array;
    }

    @Override
    public Integer call() throws Exception {
        int sum = 0;
        for (int i = startIndex; i < endIndex; i++) {
            sum += array[i];
        }

        return sum;
    }
}
