package ru.geekfactory.course.core.example._09_multithreading.part_2.atomic;

public class AtomicDemo {

    public static void main(String[] args) throws InterruptedException {
//        Counter counter = new IntCounter();
        Counter counter = new AtomicCounter();

        Thread thread_1 = new Thread(new Task(counter));
        Thread thread_2 = new Thread(new Task(counter));

        thread_1.start();
        thread_2.start();

        thread_1.join();
        thread_2.join();

        System.out.println("i = " + counter.getI());
    }

    private static class Task implements Runnable {
        private final Counter counter;

        public Task(Counter counter) {
            this.counter = counter;
        }

        @Override
        public void run() {
            for (int j = 0; j < 100_000; j++) {
                counter.increment();
            }
        }
    }
}
