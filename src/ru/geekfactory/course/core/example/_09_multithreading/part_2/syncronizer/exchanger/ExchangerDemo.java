package ru.geekfactory.course.core.example._09_multithreading.part_2.syncronizer.exchanger;

import java.util.concurrent.Exchanger;

public class ExchangerDemo implements Runnable {

    private int i;
    private Exchanger<Integer> exchanger;

    public ExchangerDemo(Exchanger<Integer> exchanger, int i) {
        this.i = i;
        this.exchanger = exchanger;
    }

    @Override
    public void run() {
        try {
            while (!Thread.currentThread().isInterrupted()) {
                int random = (int) (10000 * Math.random());
                System.out.println("My random " + i + ": " + random);
                Integer notMyRandom = exchanger.exchange(random);
                System.out.println("Not my random " + i + ": " + notMyRandom);
                Thread.sleep(2000);
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    public static void main(String[] args) throws Exception {
        Exchanger<Integer> ex = new Exchanger<>();

        Thread thread_1 = new Thread(new ExchangerDemo(ex, 0));
        Thread thread_2 = new Thread(new ExchangerDemo(ex, 1));

        thread_1.start();
        thread_2.start();

        Thread.sleep(10000);

        thread_1.interrupt();
        thread_2.interrupt();
    }
}
