package ru.geekfactory.course.core.example._09_multithreading.part_2.syncronizer.semaphore;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

/**
 * Могут работать только 5 потоков.
 */
public class SemaphoreDemo implements Runnable {
    private int i;
    private Semaphore sem;

    public SemaphoreDemo(Semaphore sem, int i) {
        this.i = i;
        this.sem = sem;
    }

    @Override
    public void run() {
        try {
            while (!Thread.currentThread().isInterrupted()) {
                sem.acquire();
                try {
                    System.out.println(LocalDateTime.now() + " Executing " + i);
                    Thread.sleep(2000);
                } finally {
                    sem.release();
                }
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    public static void main(String[] args) throws Exception {
        Semaphore sem = new Semaphore(6, true);

        List<Thread> threadList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Thread thread = new Thread(new SemaphoreDemo(sem, i));
            threadList.add(thread);
            thread.start();
        }
        Thread.sleep(10000);

        threadList.forEach(Thread::interrupt);
    }
}
