package ru.geekfactory.course.core.example._09_multithreading.part_2.executor.callable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Посчитать многопоточно сумму чисел в массиве
 */
public class CallableDemo {

    private final static int TASK_COUNT = 100;
    private final static int ARRAY_SIZE = 1_000_000_000;
    private final static int THREAD_COUNT = 10;

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        byte[] array = initArray();

//   ************ MULTI-THREAD ************
        long startTimeMultiThread = System.currentTimeMillis();

        ExecutorService threadPool = Executors.newFixedThreadPool(THREAD_COUNT);

        List<Future<Integer>> futureList = new ArrayList<>(TASK_COUNT);

        int batchSize = array.length / TASK_COUNT;
        for (int i = 0; i < TASK_COUNT; i++) {
            Future<Integer> future = threadPool.submit(new Summator(i * batchSize, (i + 1) * batchSize, array));
            futureList.add(future);
        }

        int sumMultiThread = 0;
        for (Future<Integer> future : futureList) {
            sumMultiThread += future.get();
        }

        long endTimeMultiThread = System.currentTimeMillis();

        System.out.println("Result = " + sumMultiThread + ", time = " + (endTimeMultiThread - startTimeMultiThread));
        threadPool.shutdown();

//   ************ SINGLE-THREAD ************
        long startTimeSingleThread = System.currentTimeMillis();

        int sum = 0;
        for (int i : array) {
            sum += i;
        }

        long endTimeSingleThread = System.currentTimeMillis();

        System.out.println("Result = " + sum + ", time = " + (endTimeSingleThread - startTimeSingleThread));
    }

    private static byte[] initArray() {
        byte[] array = new byte[ARRAY_SIZE];

        for (int i = 0; i < array.length; i++) {
            array[i] = (byte) (100 * Math.random());
        }

        return array;
    }
}
