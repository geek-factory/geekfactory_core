package ru.geekfactory.course.core.example._09_multithreading.part_2.atomic;

public interface Counter {

    void increment();

    int getI();
}
