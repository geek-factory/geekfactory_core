package ru.geekfactory.course.core.example._04_object.to_string;

public class Main {
    public static void main(String[] args) {
        User1 user1 = new User1();
        System.out.println(user1);

        User2 user2 = new User2();
        System.out.println(user2);
    }
}
