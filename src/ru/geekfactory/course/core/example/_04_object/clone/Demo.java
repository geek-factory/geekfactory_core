package ru.geekfactory.course.core.example._04_object.clone;

public class Demo {

    public static void main(String[] args) throws CloneNotSupportedException {
        B b = new B(1);
        A original = new A("original", b);

        A clone = original.clone();
        System.out.println("original.getB().getNum() = " + original.getB().getNum());
        System.out.println("clone.getB().getNum() = " + clone.getB().getNum());

        clone.getB().setNum(2);
        System.out.println("original.getB().getNum() = " + original.getB().getNum());
        System.out.println("clone.getB().getNum() = " + clone.getB().getNum());
    }

}
