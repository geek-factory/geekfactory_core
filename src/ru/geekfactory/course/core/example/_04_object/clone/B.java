package ru.geekfactory.course.core.example._04_object.clone;

public class B implements Cloneable {

    private int num;

    public B(int num) {
        this.num = num;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    @Override
    public B clone() throws CloneNotSupportedException {
        return (B) super.clone();
    }
}
