package ru.geekfactory.course.core.example._04_object.clone;

public class A implements Cloneable {

    private String str;
    private B b;

    public A(String str, B b) {
        this.str = str;
        this.b = b;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    public B getB() {
        return b;
    }

    public void setB(B b) {
        this.b = b;
    }

    @Override
    public A clone() throws CloneNotSupportedException {
        // Deep copy
//        A aClone = (A) super.clone();
//
//        B bClone = b.clone();
//        aClone.setB(bClone);
//
//        return aClone;

        //shallow copy
        return (A) super.clone();
    }
}
