package ru.geekfactory.course.core.example._04_object.equals_hashcode;

public class Main {
    public static void main(String[] args) {
        User user1 = new User("Иванов", "Иван", "Иванович");
        User user2 = new User("Петров", "Петр", "Петрович");
        User user3 = new User("Иванов", "Иван", "Иванович");


        System.out.println(user1.equals(user2));
        System.out.println("user1 hashCode=" + user1.hashCode() + " " + "user2  hashCode=" + user2.hashCode());

        System.out.println(user1.equals(user3));
        System.out.println("user1 hashCode=" + user1.hashCode() + " " + "user3  hashCode=" + user3.hashCode());

        System.out.println(user1.equals(null));
    }
}
