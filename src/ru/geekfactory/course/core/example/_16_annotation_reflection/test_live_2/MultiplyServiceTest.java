package ru.geekfactory.course.core.example._16_annotation_reflection.test_live_2;

public class MultiplyServiceTest {

    @Test
    public void testMultuplyWithZero() {
        MultiplyService multiplyService = new MultiplyService();

        int multiply = multiplyService.multiply(10, 0);

        assertEquals(multiply, 0);
    }

    @Test
    public void testMultuplyWithNegative() {
        MultiplyService multiplyService = new MultiplyService();

        int multiply = multiplyService.multiply(10, -10);

        assertEquals(multiply, -100);
    }

    @Test
    public void testMultuplyWithPostivie() {
        MultiplyService multiplyService = new MultiplyService();

        int multiply = multiplyService.multiply(10, 5);

        assertEquals(multiply, 10);
    }

    @Test
    public void testMultuplyException() {
        MultiplyService multiplyService = new MultiplyService();

        int multiply = multiplyService.multiply(567, 1);

        assertEquals(multiply, 567);
    }

    public static void main(String[] args) {
       TestRunner.run(MultiplyServiceTest.class);
    }

    private static void assertEquals(int actual, int expected) {
        if (actual != expected) {
            throw new AssertException("actual " + actual + " not equal expected " + expected);
        }
    }

    private static void assertEquals(Object actual, Object expected) {
        if (actual.equals(expected)) {
            throw new AssertException("actual " + actual + " not equal expected " + expected);
        }
    }

}
