package ru.geekfactory.course.core.example._16_annotation_reflection.test_live_2;

public class MultiplyService {

    public int multiply(int n1, int n2) {
        if (n1 == 567) {
            throw new RuntimeException();
        }

        return n1 * n2;
    }

}
