package ru.geekfactory.course.core.example._16_annotation_reflection.test_live;

import ru.geekfactory.course.core.example._16_annotation_reflection.test_live_2.MultiplyService;

public class MultiplyServiceTest {

    @Test
    public void testSum1() {
        MultiplyService multiplyService = new MultiplyService();

        int multiply = multiplyService.multiply(10, 10);

        assertEquals(multiply, 100);
    }

    @Test
    public void testSum2() {
        MultiplyService multiplyService = new MultiplyService();

        int multiply = multiplyService.multiply(10, 0);

        assertEquals(multiply, 0);
    }

    @Test
    public void testSum3() {
        MultiplyService multiplyService = new MultiplyService();

        int multiply = multiplyService.multiply(123, 3124);

        assertEquals(multiply, 10);
    }

    public static void main(String[] args) {
       TestRunner.run(MultiplyServiceTest.class);
//        new MultiplyServiceTest().testSum1();
//        new MultiplyServiceTest().testSum2();
//        new MultiplyServiceTest().testSum3();
    }

    private static void assertEquals(int actual, int expected) {
        if (actual != expected) {
            throw new AssertException("actual not equal to expected: " + expected);
        }
    }

}
