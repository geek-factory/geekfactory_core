package ru.geekfactory.course.core.example._16_annotation_reflection.test_live;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class TestRunner {

    public static void run(Class<?> testClass) {
        Method[] declaredMethods = testClass.getDeclaredMethods();
        List<Method> testMethods = getTestMethods(declaredMethods);

        List<Method> successTests = new ArrayList<>();
        List<Method> failedTests = new ArrayList<>();

        for (Method testMethod : testMethods) {
            try {
                Object o = testClass.newInstance();
                testMethod.invoke(o);
                successTests.add(testMethod);
            } catch (IllegalAccessException e) {
                System.out.println("Ошибка доступа к методу " + testMethod.getName());
            } catch (InvocationTargetException e) {
                Throwable cause = e.getCause();
                if (cause instanceof AssertException) {
                    failedTests.add(testMethod);
                }
                System.out.println("Метод " + testMethod.getName() + " бросил исключение " + cause);
            } catch (InstantiationException e) {
                throw new TestException("Ошибка создания объекта класса " + testClass.getSimpleName());
            }
        }

        printResult(successTests, failedTests, testMethods);
    }

    private static void printResult(List<Method> successTests, List<Method> failedTests, List<Method> testMethods) {
        System.out.println(
                "Всего тестов " + testMethods.size() + " " +
                        "Success tests " + successTests.size() + " " +
                        "Failed tests " + failedTests.size()
        );
    }

    private static List<Method> getTestMethods(Method[] declaredMethods) {
        List<Method> testMethods = new ArrayList<>();

        for (Method declaredMethod : declaredMethods) {
            Test annotation = declaredMethod.getAnnotation(Test.class);
            if (annotation != null) {
                testMethods.add(declaredMethod);
            }
        }

        return testMethods;
    }
}
