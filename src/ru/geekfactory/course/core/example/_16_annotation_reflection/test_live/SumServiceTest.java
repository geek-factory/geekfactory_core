package ru.geekfactory.course.core.example._16_annotation_reflection.test_live;

public class SumServiceTest {

    @Test
    public void testSum1() {
        SumService sumService = new SumService();

        int sum = sumService.sum(10, 10);

        assertEquals(sum, 20);
    }

    @Test
    public void testSum2() {
        SumService sumService = new SumService();

        int sum = sumService.sum(10, 0);

        assertEquals(sum, 10);
    }

    @Test
    public void testSum3() {
        SumService sumService = new SumService();

        int sum = sumService.sum(123, 3124);

        assertEquals(sum, 10);
    }

    public static void main(String[] args) {
       TestRunner.run(SumServiceTest.class);
//        new SumServiceTest().testSum1();
//        new SumServiceTest().testSum2();
//        new SumServiceTest().testSum3();
    }

    private static void assertEquals(int actual, int expected) {
        if (actual != expected) {
            throw new AssertException("actual not equal to expected: " + expected);
        }
    }

}
