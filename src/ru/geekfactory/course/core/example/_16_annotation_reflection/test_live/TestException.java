package ru.geekfactory.course.core.example._16_annotation_reflection.test_live;

public class TestException extends RuntimeException {

    public TestException(String message) {
        super(message);
    }
}
