package ru.geekfactory.course.core.example._16_annotation_reflection.test_live;

public class SumService {

    public int sum(int n1, int n2) {
        return n1 + n2;
    }

}
