package ru.geekfactory.course.core.example._16_annotation_reflection.annotation;

public class OverideDemo {

    public static void main(String[] args) {
        ChildClass childClass = new ChildClass();
        childClass.method("13");
    }


    public static class SuperClass {
        /**
         * При изменении метода, @Override подсветить ошибку
         */
        public void method(String arg) {
            System.out.println("SuperClass");
        }
    }

    public static class ChildClass extends SuperClass {

        @Override
        public void method(String arg) {
            System.out.println("ChildClass");
        }
    }
}
