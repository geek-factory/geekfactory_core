package ru.geekfactory.course.core.example._16_annotation_reflection.annotation;

public class SuppressWarningDemo {

    /**
     * При компиляции не будет warning
     */
    @SuppressWarnings("deprecation")
    public static void main(String[] args) {
        new Thread().stop();
    }
}
