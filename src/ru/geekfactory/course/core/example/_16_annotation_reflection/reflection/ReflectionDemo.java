package ru.geekfactory.course.core.example._16_annotation_reflection.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class ReflectionDemo {

    public static void main(String[] args) {
        Class<Organisation> organisationClass = Organisation.class;

        printPackage(organisationClass);
        printClassName(organisationClass);
        printClassModifier(organisationClass);
        printClassSuperClass(organisationClass);
        printClassInterfaces(organisationClass);
        printClassFields(organisationClass);
    }

    private static void printClassFields(Class<Organisation> organisationClass) {
        Field[] fields = organisationClass.getDeclaredFields();
        String str = "";
        for (Field field : fields) {
            str += getModifier(field.getModifiers()) + " " + field.getType().getSimpleName() + " " +  field.getName() + "\n";
        }

        System.out.println(str);
    }

    private static void printClassInterfaces(Class<Organisation> organisationClass) {
        Class<?>[] interfaces = organisationClass.getInterfaces();
        String str = "interfaces [ ";
        for (Class<?> anInterface : interfaces) {
            str += anInterface.getSimpleName() + " ";
        }

        str += "]";
        System.out.println(str);
    }

    private static void printClassSuperClass(Class<Organisation> organisationClass) {
        Class<? super Organisation> superclass = organisationClass.getSuperclass();
        System.out.println("superClass " + superclass.getSimpleName());
    }

    private static void printPackage(Class<?> clazz) {
        Package aPackage = clazz.getPackage();

        String packageName = aPackage.getName();
        System.out.println("package " + packageName);
    }

    private static void printClassName(Class<?> organisationClass) {
        System.out.println("className " + organisationClass.getName());
        System.out.println("classSimpleName " + organisationClass.getSimpleName());
    }

    private static void printClassModifier(Class<?> organisationClass) {
        int modifiers = organisationClass.getModifiers();
        System.out.println("class modifier " + getModifier(modifiers));
    }

    private static String getModifier(int m) {
        if (Modifier.isPublic(m)) return "public";
        if (Modifier.isProtected(m)) return "protected";
        if (Modifier.isPrivate(m)) return "private";
        if (Modifier.isStatic(m)) return "static";
        if (Modifier.isAbstract(m)) return "abstract";
        else return "";
    }
}
