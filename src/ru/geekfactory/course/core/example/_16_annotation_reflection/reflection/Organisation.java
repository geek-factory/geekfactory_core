package ru.geekfactory.course.core.example._16_annotation_reflection.reflection;

import ru.geekfactory.course.core.example._16_annotation_reflection.tasks.Documentation;

import java.time.LocalDate;

@Documentation(
        fullDoc = "Полное описание класса организации",
        shortDoc = "короткое описание класса организации"
)
public class Organisation implements Cloneable {

    private String name;
    private String address;
    private LocalDate dateOfCreation;

    public Organisation(String name, String address, LocalDate dateOfCreation) {
        this.name = name;
        this.address = address;
        this.dateOfCreation = dateOfCreation;
    }

    public Organisation(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LocalDate getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(LocalDate dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    @Override
    public String toString() {
        return "Organisation{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", dateOfCreation=" + dateOfCreation +
                '}';
    }

    public void method() {
//        System.out.println("Меня вызвали!!");
        throw new RuntimeException("Ошибка вызова!");
    }
}
