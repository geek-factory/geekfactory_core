package ru.geekfactory.course.core.example._16_annotation_reflection.reflection;

import ru.geekfactory.course.core.example._16_annotation_reflection.tasks.Documentation;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDate;

public class ReflectionDemo2 {

    public static void main(String[] args) throws Throwable {
//        printDocumentation(Organisation.class);

        Organisation organisation = new Organisation("Рога и Копыта", "Неизвестно", LocalDate.of(2018, 1, 1));
//        System.out.println(organisation);

//        changePrivateField(organisation, "name", "сломали private");
//        System.out.println(organisation);

        invokeMethod(organisation, "method");
    }

    private static void invokeMethod(Object organisation, String methodName) throws Throwable {
        Class<?> aClass = organisation.getClass();
        try {
            Method method = aClass.getMethod(methodName);
            method.invoke(organisation);
        } catch (NoSuchMethodException e) {
            System.out.println("Метод не существует " + methodName);
        } catch (InvocationTargetException e) {
            System.out.println("Метод бросил исключение");
            throw e.getCause();
        } catch (IllegalAccessException e) {
            System.out.println("Ошибка доступа к методу " + methodName);
        }
    }

    private static void changePrivateField(Organisation organisation, String name, String newValue) {
        Class<?> aClass = organisation.getClass();
        try {
            Field field = aClass.getDeclaredField(name);
            field.setAccessible(true);
            field.set(organisation, newValue);
        } catch (NoSuchFieldException e) {
            System.out.println("Нету такого поля");
        } catch (IllegalAccessException e) {
            System.out.println("Ошибка доступа к полю " + name);
        }
    }

    private static void printDocumentation(Class<?> clazz) {
        Documentation annotation = clazz.getAnnotation(Documentation.class);
        if (annotation == null) return;

        String fullDoc = annotation.fullDoc();
        String shortDoc = annotation.shortDoc();

        System.out.println("fullDoc = " + fullDoc);
        System.out.println("shortDoc = " + shortDoc);
    }
}
