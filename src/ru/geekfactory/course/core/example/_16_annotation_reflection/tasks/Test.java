package ru.geekfactory.course.core.example._16_annotation_reflection.tasks;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Test {

    Class<? extends Throwable> expected() default NoneThrowable.class;

    class NoneThrowable extends Throwable {}
}
