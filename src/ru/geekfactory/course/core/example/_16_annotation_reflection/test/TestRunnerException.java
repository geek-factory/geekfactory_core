package ru.geekfactory.course.core.example._16_annotation_reflection.test;

public class TestRunnerException extends RuntimeException {

    public TestRunnerException(String message, Throwable cause) {
        super(message, cause);
    }
}
