package ru.geekfactory.course.core.example._16_annotation_reflection.test.asserts;

import java.util.Objects;

public class Assert {

    public static void assertEquals(Object expected, Object actual) {
        boolean equals = Objects.equals(expected, actual);
        if (!equals) {
            throw new AssertException("Objects not equals. Actual: {" + actual + "}. But expected: {" + expected + "}");
        }
    }
}
