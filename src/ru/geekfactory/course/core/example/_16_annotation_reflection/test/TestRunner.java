package ru.geekfactory.course.core.example._16_annotation_reflection.test;

import ru.geekfactory.course.core.example._16_annotation_reflection.test.annotation.Test;
import ru.geekfactory.course.core.example._16_annotation_reflection.test.asserts.AssertException;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class TestRunner {

    public static void run(Class testClass) {
        List<TestResult> failedTests = new ArrayList<>();
        List<TestResult> successTests = new ArrayList<>();

        Method[] methods = testClass.getDeclaredMethods();
        List<Method> testMethods = getTestMethods(methods);

        for (Method testMethod : testMethods) {
            Test annotation = testMethod.getAnnotation(Test.class);
            Class<? extends Throwable> expected = annotation.expected();

            try {
                Object o = testClass.newInstance();
                testMethod.invoke(o);
                if (expected.isAssignableFrom(Test.NoneThrowable.class)) {
                    successTests.add(new TestResult(testMethod));
                }else if (!expected.isAssignableFrom(Test.NoneThrowable.class)) {
                    failedTests.add(new TestResult(testMethod, new AssertException("expected exception " + expected.getSimpleName())));
                }
            } catch (InstantiationException e) {
                throw new TestRunnerException("Error instantiate test class", e);
            } catch (IllegalAccessException e) {
                throw new TestRunnerException("Error access test method", e);
            } catch (InvocationTargetException e) {
                if (e.getTargetException() instanceof AssertException) {
                    failedTests.add(new TestResult(testMethod, (AssertException) e.getTargetException()));
                } else {
                    if (expected.isAssignableFrom(Test.NoneThrowable.class)) {
                        failedTests.add(new TestResult(testMethod, new AssertException("Test method throw exception " + e.getTargetException(), e.getTargetException())));
                    } else {
                        if (e.getTargetException().getClass().isAssignableFrom(expected)) {
                            successTests.add(new TestResult(testMethod));
                        } else {
                            failedTests.add(new TestResult(
                                    testMethod,
                                    new AssertException("Test method throw exception " + e.getTargetException().getClass().getSimpleName() +
                                            " but expected " + expected.getSimpleName(), e)));
                        }
                    }
                }
            }
        }

        System.out.println("Total tests: " + testMethods.size() + ". Failed tests: " + failedTests.size() + ". Success tests: " + successTests.size());
        failedTests.forEach(System.err::println);
    }

    private static List<Method> getTestMethods(Method[] methods) {
        List<Method> testMethods = new ArrayList<>();
        for (Method method : methods) {
            for (Annotation annotation : method.getAnnotations()) {
                if (Test.class.equals(annotation.annotationType())) {
                    testMethods.add(method);
                }
            }
        }

        return testMethods;
    }

    private static class TestResult {
        private Method testMethod;
        private AssertException exception;

        TestResult(Method testMethod, AssertException exception) {
            this.testMethod = testMethod;
            this.exception = exception;
        }

        TestResult(Method testMethod) {
            this.testMethod = testMethod;
        }

        @Override
        public String toString() {
            return "AssertException: " + exception.getMessage() +
                    ". TestMethod: " + testMethod.getName() + "()";
        }
    }
}
