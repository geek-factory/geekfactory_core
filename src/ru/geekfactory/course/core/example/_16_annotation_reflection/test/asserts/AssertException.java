package ru.geekfactory.course.core.example._16_annotation_reflection.test.asserts;

public class AssertException extends RuntimeException {

    public AssertException(String message) {
        super(message);
    }

    public AssertException(String message, Throwable cause) {
        super(message, cause);
    }
}
