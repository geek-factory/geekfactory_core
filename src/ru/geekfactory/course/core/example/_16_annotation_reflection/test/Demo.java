package ru.geekfactory.course.core.example._16_annotation_reflection.test;

import ru.geekfactory.course.core.example._16_annotation_reflection.test.annotation.Test;
import ru.geekfactory.course.core.example._16_annotation_reflection.test.asserts.Assert;

import java.io.IOException;

public class Demo {

    @Test
    public void someTest() {
        Assert.assertEquals(1, 1);
    }

    @Test(expected = IOException.class)
    public void someTestWithException() {
        Assert.assertEquals(1, 1);
    }

    public static void main(String[] args) {
        TestRunner.run(Demo.class);
    }
}
