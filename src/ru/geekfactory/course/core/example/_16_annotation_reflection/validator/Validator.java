package ru.geekfactory.course.core.example._16_annotation_reflection.validator;

import ru.geekfactory.course.core.example._16_annotation_reflection.validator.annotation.DateSince;
import ru.geekfactory.course.core.example._16_annotation_reflection.validator.annotation.NotEmpty;
import ru.geekfactory.course.core.example._16_annotation_reflection.validator.annotation.Valid;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.time.LocalDate;

public class Validator {

    public boolean isValid(Object object) throws IllegalAccessException {
        Class<?> aClass = object.getClass();
        Valid annotation = aClass.getAnnotation(Valid.class);
        if (annotation == null) return true;

        Field[] declaredFields = aClass.getDeclaredFields();
        for (Field declaredField : declaredFields) {
            Annotation[] fieldAnnotations = declaredField.getDeclaredAnnotations();
            for (Annotation fieldAnnotation : fieldAnnotations) {
                Class<? extends Annotation> annotationType = fieldAnnotation.annotationType();

                if (annotationType.isAssignableFrom(NotEmpty.class)) {
                    boolean valid = notEmptyValidate(object, declaredField);
                    if (!valid) return false;
                }

                if (annotationType.isAssignableFrom(DateSince.class)) {
                    boolean valid = dateSinceValidate(object, declaredField, (DateSince) fieldAnnotation);
                    if (!valid) return false;
                }
            }

        }

        return true;
    }

    private boolean dateSinceValidate(Object object, Field field, DateSince fieldAnnotation) throws IllegalAccessException {
        field.setAccessible(true);
        Object fieldValue = field.get(object);
        Class<?> fieldType = field.getType();
        if (fieldType.isAssignableFrom(LocalDate.class)) {
            System.err.println("Invalid field " + field.getName() + " type " + fieldType);
        }
        LocalDate dateSince = LocalDate.of(fieldAnnotation.year(), fieldAnnotation.month(), fieldAnnotation.day());

        return fieldValue != null && dateSince.isBefore((LocalDate)fieldValue);
    }

    private boolean notEmptyValidate(Object object, Field field) throws IllegalAccessException {
        field.setAccessible(true);
        Object fieldValue = field.get(object);
        Class<?> type = field.getType();
        if (!type.isAssignableFrom(String.class)) {
            System.err.println("Invalid field " + field.getName() + " type " + type);
        }

        return fieldValue != null && ((String)fieldValue).length() > 0;
    }
}