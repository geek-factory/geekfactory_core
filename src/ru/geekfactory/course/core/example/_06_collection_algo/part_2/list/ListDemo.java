package ru.geekfactory.course.core.example._06_collection_algo.part_2.list;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ListDemo {

    public static void main(String[] args) {
        List<Integer> arrayList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            arrayList.add(i);
        }
        for (int i = 0; i < 10; i++) {
            System.out.print(arrayList.get(i));
        }

        System.out.println();

        List<Integer> linkedList = new LinkedList<>();
        for (int i = 0; i < 10; i++) {
            linkedList.add(i);
        }
        for (int i = 0; i < 10; i++) {
            System.out.print(linkedList.get(i));
        }

    }
}
