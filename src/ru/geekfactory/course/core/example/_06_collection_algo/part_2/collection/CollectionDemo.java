package ru.geekfactory.course.core.example._06_collection_algo.part_2.collection;

import java.util.ArrayList;
import java.util.Collection;

public class CollectionDemo {

    public static void main(String[] args) {
        // Создаем List, который является наследником Collection
        Collection<Integer> collection = new ArrayList<>();

        Integer value = 100;

        System.out.println("Before add contains - " + collection.contains(value));

        collection.add(value);

        System.out.println("After add contains - " + collection.contains(value));

        collection.remove(value);

        System.out.println("After remove contains - " + collection.contains(value));
    }
}
