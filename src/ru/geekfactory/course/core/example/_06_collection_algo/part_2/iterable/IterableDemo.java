package ru.geekfactory.course.core.example._06_collection_algo.part_2.iterable;

import java.util.Arrays;
import java.util.Iterator;

public class IterableDemo {

    public static void main(String[] args) {
        // Создаем List, который является наследником Iterable
        Iterable<Integer> iterable = Arrays.asList(1, 2, 3);

        // Итерируемся через iterator
        Iterator<Integer> iterator = iterable.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        // foreach использует iterator (в .class файле это видно)
        for (Integer anIterable : iterable) {
            System.out.println(anIterable);
        }
    }
}
