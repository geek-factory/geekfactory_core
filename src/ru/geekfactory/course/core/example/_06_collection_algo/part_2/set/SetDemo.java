package ru.geekfactory.course.core.example._06_collection_algo.part_2.set;

public class SetDemo {
    public static void main(String[] args) {
        String text = "ротор";
        textContrast(text);
        System.out.println("Является ли слово перевертышем? " + textContrast(text));
    }

    public static boolean textContrast(String text) {
        //Преобразовываем строку в массив
        char[] textArray = text.toCharArray();
        //сравниваем символы в слове
        boolean b = false;
        for (int i = text.length() - 1; !b && i >= 0; i--) {
            for (int a = 0; !b && a < i; a++) {
                if (textArray[i] != textArray[a]) {
                    return b;
                } else {
                    b = true;
                }
            }
        }
        return true;
    }
}
